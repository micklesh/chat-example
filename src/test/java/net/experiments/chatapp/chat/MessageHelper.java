package net.experiments.chatapp.chat;

import net.experiments.chatapp.model.ChatMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageHelper {
    public static List<ChatMessage> createChatMessages(ChatMessage... messages) {
        return Arrays.asList(messages);
    }

    public static ChatMessage createMessage(String messageText, String messageAuthor) {
        ChatMessage message = new ChatMessage();
        message.setMessage(messageText);
        message.setAuthor(messageAuthor);
        return message;
    }
}
