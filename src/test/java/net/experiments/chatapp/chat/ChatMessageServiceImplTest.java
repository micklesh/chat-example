package net.experiments.chatapp.chat;

import net.experiments.chatapp.model.ChatMessage;
import net.experiments.chatapp.repositories.ChatMessageRepository;
import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.List;

import static net.experiments.chatapp.chat.MessageHelper.createMessage;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ChatMessageServiceImplTest {

    private ChatMessageRepository repository;
    private ChatMessageService chatMessageService;

    @Before
    public void setUp() throws Exception {
        repository = Mockito.mock(ChatMessageRepository.class);
        chatMessageService = new ChatMessageServiceImpl(repository);
    }

    @Test
    public void getChatMessages()  {
        List<ChatMessage> chatMessages = MessageHelper.createChatMessages(new ChatMessage());
        LocalDateTime dateTime = LocalDateTime.now();
        when(repository.findTop10ByDateTimeAfterOrderByDateTimeAsc(dateTime)).thenReturn(chatMessages);

        List<ChatMessage> actual = chatMessageService.getChatMessages(dateTime);
        assertThat(actual, is(chatMessages));
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailWnehSaveEmptyMessage() throws Exception {
        chatMessageService.sendChatMessage(null);
    }

    @Test
    public void shouldEnrichFieldsForMessage() {
        ChatMessage msg = createMessage("hello", null);
        ChatMessage msgResponse = createMessage("response", "author");
        msgResponse.setDateTime(LocalDateTime.MAX);
        when(repository.save(eq(msg))).thenReturn(msgResponse);

        chatMessageService.sendChatMessage(msg);

        ArgumentCaptor<ChatMessage> argument = ArgumentCaptor.forClass(ChatMessage.class);
        verify(repository, times(1)).save(argument.capture());
        ChatMessage actual = argument.getValue();
        assertThat(actual.getAuthor(), is("Anonymous"));
        assertThat(actual.getMessage(), is("hello"));
        assertThat(actual.getDateTime(), IsNull.notNullValue());
    }
}