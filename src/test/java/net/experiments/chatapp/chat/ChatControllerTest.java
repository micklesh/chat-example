package net.experiments.chatapp.chat;

import net.experiments.chatapp.model.ChatMessage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.mockito.Matchers.eq;

public class ChatControllerTest {

    private static ChatMessage MSG1 = MessageHelper.createMessage("messageText1", "messageAuthor1");
    private static ChatMessage MSG2 = MessageHelper.createMessage("messageText2", "messageAuthor2");

    private ChatController controller;
    private ChatMessageService messageService;

    @Before
    public void setUp() throws Exception {

        messageService = Mockito.mock(ChatMessageService.class);
        this.controller = new ChatController(messageService);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void shouldReturnChatMessagesFromService() {

        List<ChatMessage> chatMessages = MessageHelper.createChatMessages(MSG1, MSG2);
        Mockito.when(messageService.getChatMessages(null)).thenReturn(chatMessages);

        List<ChatMessage> actualMessages = controller.getMessages(null);

        assertThat(actualMessages.size(), is(2));
        assertThat(actualMessages, contains(MSG1, MSG2));
    }

    @Test
    public void shouldSaveMessageSuccessfully() {
        Mockito.when(messageService.sendChatMessage(eq(MSG1))).thenReturn(MSG2);
        assertThat(controller.sendChatMessage(MSG1), equalTo(MSG2));
    }

    @Test(expected = RuntimeException.class)
    public void shouldRethrowErrorIfSendFailed() {
        Mockito.when(messageService.sendChatMessage(Mockito.anyObject())).thenThrow(new RuntimeException("DB SAVE ERROR"));
        controller.sendChatMessage(MSG1);
    }

}