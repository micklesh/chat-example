angular.module('chatapp', []).controller('MessagesController', function ($scope, $http, $interval, $filter) {
    $scope.chatmessage = {};
    $scope.messages = [];
    $scope.latestMessageTime = new Date();

    this.send = function () {
        $scope.chatmessage.author = $scope.username;
        $http.post('/chat', $scope.chatmessage).success(function (data) {
            console.log("Sending");
        });
        $scope.chatmessage = '';
        $scope.chatform.msgtext.$$element.focus();
    };

    $scope.get = function () {
        var formattedDate = $filter('date')($scope.latestMessageTime, 'yyyy-MM-dd HH:mm:ss.sss');
        this.resp = $http.get('/chat?dateTime='+formattedDate).success(function (data) {
            var maxCount = 50;
            $scope.messages = $scope.messages.concat(data);

            var currentCount = $scope.messages.length;
            var latestMessage = $scope.messages[currentCount - 1];
            if(latestMessage !== undefined) {
                $scope.latestMessageTime = latestMessage.dateTime;
            }
            if(currentCount > maxCount ) {
                $scope.messages.splice(0, currentCount - maxCount);
            }
        });
    };

    var refreshChat = function() {
       $scope.get();
    };

    var chatInterval = $interval(refreshChat, 2000);
    $scope.$on('$destroy', function(){
        $interval.cancel(chatInterval)
    });

});