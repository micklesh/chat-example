package net.experiments.chatapp.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import net.experiments.chatapp.util.DateUtils;

import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatMessage {

    @JsonProperty("message")
    private String message;

    @JsonProperty("author")
    private String author;

    @JsonProperty("dateTime")
    @JsonFormat(pattern = DateUtils.DATE_TIME_FORMAT)
    private LocalDateTime dateTime;
}
