package net.experiments.chatapp.chat;

import net.experiments.chatapp.model.ChatMessage;

import java.time.LocalDateTime;
import java.util.List;

public interface ChatMessageService {
    List<ChatMessage> getChatMessages(LocalDateTime dateTime);
    ChatMessage sendChatMessage(ChatMessage chatMessage);
}
