package net.experiments.chatapp.chat;

import net.experiments.chatapp.model.ChatMessage;
import net.experiments.chatapp.repositories.ChatMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.util.StringUtils.isEmpty;

@Service
public class ChatMessageServiceImpl implements ChatMessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatMessageServiceImpl.class);

    private ChatMessageRepository chatMessageRepository;

    @Autowired
    public ChatMessageServiceImpl(ChatMessageRepository chatMessageRepository) {
        this.chatMessageRepository = chatMessageRepository;
    }

    @Override
    public List<ChatMessage> getChatMessages(LocalDateTime dateTime) {
        LOGGER.debug("Getting all the messages from repository");
        return chatMessageRepository.findTop10ByDateTimeAfterOrderByDateTimeAsc(dateTime);
    }

    @Override
    public ChatMessage sendChatMessage(ChatMessage chatMessage) {
        enrichMessage(chatMessage);
        LOGGER.debug("Trying to save a message");
        return chatMessageRepository.save(chatMessage);
    }

    private void enrichMessage(ChatMessage chatMessage) {
        chatMessage.setDateTime(LocalDateTime.now());
        if(isEmpty(chatMessage.getAuthor())) {
            chatMessage.setAuthor("Anonymous");
        }
    }
}
