package net.experiments.chatapp.chat;

import net.experiments.chatapp.model.ChatMessage;
import net.experiments.chatapp.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class ChatController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatController.class);

    private ChatMessageService messageService;

    @Autowired
    public ChatController(ChatMessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping(value = "/chat", method = RequestMethod.GET)
    public List<ChatMessage> getMessages(@RequestParam("dateTime") @DateTimeFormat(pattern = DateUtils.DATE_TIME_FORMAT) LocalDateTime dateTime) {
        return messageService.getChatMessages(dateTime);
    }

    @RequestMapping(value = "/chat", method = RequestMethod.POST)
    public ChatMessage sendChatMessage(@RequestBody ChatMessage chatMessage) {
        return messageService.sendChatMessage(chatMessage);
    }
}
