package net.experiments.chatapp.configuration;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.net.UnknownHostException;
import java.util.Arrays;

@Configuration
@EnableMongoRepositories(basePackages = {"net.experiments.chatapp.repositories"})
public class MongoConfiguration {

    @Value("${mongo.address}")
    String mongoAddress;

    @Value("${mongo.db.name}")
    String mongoDbName;

    @Bean
    public MongoClient mongo() throws UnknownHostException {
        return new MongoClient(Arrays.asList(new ServerAddress(mongoAddress)));
    }

    @Bean
    public MongoDbFactory mongoDbFactory(MongoClient mongoClient) throws Exception {
        return new SimpleMongoDbFactory(mongoClient, mongoDbName);
    }

    @Bean
    public MongoTemplate mongoTemplate(MongoDbFactory factory) throws Exception {
        return new MongoTemplate(factory);
    }
}
