package net.experiments.chatapp.repositories;

import net.experiments.chatapp.model.ChatMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ChatMessageRepository extends MongoRepository<ChatMessage, String> {
    List<ChatMessage> findTop10ByDateTimeAfterOrderByDateTimeAsc(LocalDateTime lastMessage);
}
