# README #

### What is this repository for? ###

* Just a simple **unsecured** chat service

### How do I get set up? ###

* Jdk 8 and MongoDB required to build/run
* mongo db url is specified in application.properties file in format HOST:PORT
* In order to build  the server execute **./gradlew clean build** 
* In order to run the server execute **java -jar ./build/libs/chat-example-0.0.1-SNAPSHOT.jar** 
* Access the chat page: http://localhost:8080/ by default, add -Dserver.port=<port> to startup clause to change it